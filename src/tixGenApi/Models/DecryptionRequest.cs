﻿using System.IO;

namespace tixGenApi.Models
{
    public class DecryptionRequest
    {
        public string CypherText { get; set; }
    }
}
