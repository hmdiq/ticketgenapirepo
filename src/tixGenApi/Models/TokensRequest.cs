﻿using System.Collections.Generic;

namespace tixGenApi.Models
{
    public class TokensRequest
    {
        public TokensRequest()
        {
            Request = new List<TokenRequest>();
        }

        public List<TokenRequest> Request { get; set; }
    }
}
