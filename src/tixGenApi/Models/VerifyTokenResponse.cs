﻿namespace tixGenApi.Models
{
    public class VerifyTokenResponse
    {
        public bool IsVerified { get; set; }
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
    }
}
