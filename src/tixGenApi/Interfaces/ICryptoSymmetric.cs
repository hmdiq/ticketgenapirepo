﻿using System.Threading.Tasks;
using tixGenApi.Models;

namespace tixGenApi.Interfaces
{
    public interface ICryptoSymmetric
    {
        Task<EncryptionResponse> DataProtect(EncryptionRequest request);
        Task<DecryptionResponse> DataUnProtect(DecryptionRequest request);
    }
}
