﻿namespace tixGenApi.Models
{
    public class GenerateTicketResponse
    {
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
        public string QrCode { get; set; }

        public string TicketType { get; set; }
        public string TicketPrice { get; set; }

        public GeneratedTokenModel GeneratedToken { get; set; }
    }
}
