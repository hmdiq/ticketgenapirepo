﻿using System.Drawing;

namespace tixGenApi.Models
{
    public class WriteQrCodesResponse
    {
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
        public Bitmap QrCodeBitmap { get; set; }
    }
}
