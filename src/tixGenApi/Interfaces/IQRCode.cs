﻿using System.Threading.Tasks;
using tixGenApi.Models;

namespace tixGenApi.Interfaces
{
    public interface IQrCode
    {
        Task<WriteQrCodeResponse> WriteQrCode(WriteQrCodeRequest request);
        Task<WriteQrCodesResponse> WriteQrCodes(WriteQrCodesRequest request);
    }
}
