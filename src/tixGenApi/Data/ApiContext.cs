﻿using Microsoft.EntityFrameworkCore;
using tixGenApi.Models;

namespace tixGenApi.Data
{
    public class ApiContext : DbContext
    {
        public ApiContext()
        {
            
        }

        public ApiContext(DbContextOptions<ApiContext> options) : base(options)
        {
            
        }

        public DbSet<GeneratedTokenModel> Tokens { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GeneratedTokenModel>().ToTable("Tokens");
        }
    }
}
