﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Azure.KeyVault;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Serialization;
using tixGenApi.Config;
using tixGenApi.Data;
using tixGenApi.Interfaces;
using tixGenApi.Models;
using tixGenApi.Services;
using tixGenApi.Utilities;

namespace tixGenApi
{
    public class Startup
    {
        private TokenAuthOptions _tokenOptions;
        public const string TokenAudience = "ExampleAudience";
        public const string TokenIssuer = "CN=ticketgeneratorcert";

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
                .AddJsonFile("certsettings.json", true, true)
                .AddJsonFile("azurekeyvault.json", false, true)
                .AddJsonFile($"azurekeyvault.{env.EnvironmentName}.json", false, true)
                .AddEnvironmentVariables();

            var config = builder.Build();
            builder.AddAzureKeyVault(
                $"https://{config["azureKeyVault:vault"]}.vault.azure.net/",
                config["azureKeyVault:clientId"],
                config["azureKeyVault:clientSecret"]
            );

            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddOptions();
            services.Configure<CertificateValidationConfig>(Configuration.GetSection("CertificateValidation"));

            services.AddDbContext<ApiContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));


            var url = Configuration["azureKeyVault:vault"];
            url = $"https://{url}.vault.azure.net/";

            var clientId = Configuration["azureKeyVault:clientId"];
            var secret = Configuration["azureKeyVault:clientSecret"];

            const string secretIdentifier = "https://keyvault-tokendispenser.vault.azure.net/secrets/kvcert/f0d6c87823d74d97821f4abecfc00ca1";

            var cert2 = GetCertificateKeyVault(secretIdentifier, url, clientId, secret).Result;

            var signingKey = new X509SecurityKey(cert2);

            _tokenOptions = new TokenAuthOptions()
            {
                Audience = TokenAudience,
                Issuer = TokenIssuer,
                SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.RsaSha256Signature)
            };

            services.AddSingleton(_tokenOptions);
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // Add framework services.
            services.AddMvc(config =>
            {
                config.RespectBrowserAcceptHeader = true;
                config.InputFormatters.Add(new XmlSerializerInputFormatter());
                config.OutputFormatters.Add(new XmlSerializerOutputFormatter());
            }).AddJsonOptions(opts =>
            {
                opts.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                opts.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });

            // Add application services.
            services.AddTransient<IEmailSender, AuthMessageSender>();

            services.AddSingleton<IConfiguration>(Configuration);
            services.AddTransient<ICryptoSymmetric, CryptoSymmetricService>();
            services.AddTransient<IQrCode, QrCodeService>();
            services.AddTransient<ITokens, TokenService>();
            services.AddTransient<ITickets, TicketingService>();
            services.AddTransient<IRepository<GeneratedTokenModel>, Repository>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseJwtBearerAuthentication(
                new JwtBearerOptions
                {
                    Audience = "https://r1eteriyahoo.onmicrosoft.com/8acaf40b-aef8-4338-92f5-63b9444477ee",
                    
                    Authority = "https://login.microsoftonline.com/r1eteriyahoo.onmicrosoft.com",
                    TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true
                    }
                });

            app.UseMvc();
        }

        private static async Task<X509Certificate2> GetCertificateKeyVault(string secretIdentifier, string url, string clientId, string secret)
        {
            var util = new KeyVaultUtil(url, clientId, secret);
            var client = new KeyVaultClient(util.GetAccessToken);
            var vaultSecret = await client.GetSecretAsync(secretIdentifier);

            var certSecret = new X509Certificate2(
                Convert.FromBase64String(vaultSecret.Value),
                string.Empty,
                X509KeyStorageFlags.MachineKeySet);
            return certSecret;
        }
    }
}
