﻿using System;

namespace tixGenApi.Models
{
    public class TokenRequest
    {
        public string UserEmail { get; set; }
        public DateTime? Expires { get; set; }

        public string TicketType { get; set; }
        public string TicketPrice { get; set; }

        public string EventName { get; set; }
    }
}
