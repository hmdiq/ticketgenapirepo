﻿namespace tixGenApi.Models
{
    public class WriteQrCodeRequest
    {
        public string TokenPlainText { get; set; }
    }
}
