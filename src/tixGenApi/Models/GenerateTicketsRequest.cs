﻿using System.Collections.Generic;

namespace tixGenApi.Models
{
    public class GenerateTicketsRequest
    {
        public GenerateTicketsRequest()
        {
            Requests = new List<TokenRequest>();
        }
        public List<TokenRequest> Requests { get; set; }
    }
}
