﻿namespace tixGenApi.Models
{  
    public class TokenResponse
    {
        public string Token { get; set; }
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
        public string TicketType { get; set; }
        public string TicketPrice { get; set; }
        public GeneratedTokenModel GeneratedToken { get; set; }
    }
}
