﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace tixGenApi.Migrations
{
    public partial class newmodelinfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Tokens",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Event",
                table: "Tokens",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Email",
                table: "Tokens");

            migrationBuilder.DropColumn(
                name: "Event",
                table: "Tokens");
        }
    }
}
