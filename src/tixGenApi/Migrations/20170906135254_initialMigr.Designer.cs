﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using tixGenApi.Data;

namespace tixGenApi.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20170906135254_initialMigr")]
    partial class initialMigr
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("tixGenApi.Models.GeneratedTokenModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Created");

                    b.Property<DateTime>("Modified");

                    b.Property<string>("Token");

                    b.Property<bool>("Valid");

                    b.HasKey("Id");

                    b.ToTable("Tokens");
                });
        }
    }
}
