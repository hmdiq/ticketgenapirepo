﻿using System.Collections.Generic;

namespace tixGenApi.Models
{
    public class WriteQrCodesRequest
    {
        public IEnumerable<string> Tokens { get; set; }
    }
}
