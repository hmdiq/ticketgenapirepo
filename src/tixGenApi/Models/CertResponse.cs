﻿namespace tixGenApi.Models
{
    public class CertResponse
    {
        public string Certificate { get; set; }
    }
}
