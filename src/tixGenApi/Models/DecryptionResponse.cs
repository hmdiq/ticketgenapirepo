﻿namespace tixGenApi.Models
{
    public class DecryptionResponse
    {
        public string DecryptedContent { get; set; }
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
    }
}
