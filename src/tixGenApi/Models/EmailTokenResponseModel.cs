﻿namespace tixGenApi.Models
{
    public class EmailTokenResponseModel
    {
        public string Token { get; set; }
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
    }
}
