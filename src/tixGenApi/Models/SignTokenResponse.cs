﻿namespace tixGenApi.Models
{
    public class SignTokenResponse
    {
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
        public string SignedToken { get; set; }
    }
}
