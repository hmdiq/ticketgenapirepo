﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using tixGenApi.Interfaces;
using tixGenApi.Models;

namespace tixGenApi.Services
{
    public class CryptoSymmetricService : ICryptoSymmetric
    {

        private readonly ILogger<CryptoSymmetricService> _logger;
        private IHostingEnvironment _environment;
        private IDataProtector _protector;

        public CryptoSymmetricService(ILogger<CryptoSymmetricService> logger,
                             IDataProtectionProvider provider,
                             IHostingEnvironment environment)
        {
            _logger = logger;
            _environment = environment;
            _protector = provider.CreateProtector(GetType().FullName);
        }

        public async Task<EncryptionResponse> DataProtect(EncryptionRequest request)
        {
            try
            {
                var cipherText = _protector.Protect(request.TokenPlainText);
                await Task.Delay(1);
                if(string.IsNullOrEmpty(cipherText))
                {
                    return new EncryptionResponse
                    {
                        IsError = true,
                        ErrorMessage = "Unable to protect data",
                        CypherText = string.Empty
                    };
                }
                else
                {
                    return new EncryptionResponse
                    {
                        IsError = false,
                        ErrorMessage = string.Empty,
                        CypherText = cipherText
                    };
                }
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to protect data", exception.ToString());
                return new EncryptionResponse
                {
                    CypherText = string.Empty,
                    ErrorMessage = "Error while trying to protect data",
                    IsError = true
                };
            }
        }

        public async Task<DecryptionResponse> DataUnProtect(DecryptionRequest request)
        {
            try
            {
                var plainText = _protector.Unprotect(request.CypherText);
                await Task.Delay(1);
                if (string.IsNullOrEmpty(plainText))
                {
                    return new DecryptionResponse
                    {
                        IsError = true,
                        ErrorMessage = "Unable to unprotect data",
                        DecryptedContent = string.Empty
                    };
                }
                else
                {
                    return new DecryptionResponse
                    {
                        IsError = false,
                        ErrorMessage = string.Empty,
                        DecryptedContent = plainText
                    };
                }
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to unprotect data", exception.ToString());
                return new DecryptionResponse
                {
                    DecryptedContent = string.Empty,
                    ErrorMessage = "Error while trying to unprotect data",
                    IsError = true
                };
            }
        }
       
    }
}
