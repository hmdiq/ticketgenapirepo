﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.Azure.KeyVault;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using tixGenApi.Interfaces;
using tixGenApi.Models;
using tixGenApi.Utilities;

namespace tixGenApi.Services
{
    public class TokenService : ITokens
    {
        private readonly TokenAuthOptions _tokenOptions;
        private readonly ILogger<TokenService> _logger;
        private readonly IRepository<GeneratedTokenModel> _repository;
        private IConfiguration Configuration { get; set; }

        public TokenService(TokenAuthOptions tokenOptions,
                            ILogger<TokenService> logger,
            IRepository<GeneratedTokenModel> repository,
                            IConfiguration configuration)
        {
            _tokenOptions = tokenOptions;
            _logger = logger;
            Configuration = configuration;
            _repository = repository;
        }

        public async Task<TokenResponse> GenerateTokenAsync(TokenRequest request)
        {
            try
            {
                var handler = new JwtSecurityTokenHandler();
                var uid = DateTime.Now.Ticks.ToString();

                // Here, you should create or look up an identity for the user which is being authenticated.
                // For now, just creating a simple generic identity.
                var identity = new ClaimsIdentity(new GenericIdentity(request.UserEmail, "UserEmail"),
                    new[]
                    {
                        new Claim("Event", request.EventName, ClaimValueTypes.String),
                        new Claim("Date", DateTime.Now.ToString(), ClaimValueTypes.DateTime),
                        new Claim("Uid",uid , ClaimValueTypes.String)
                    });

                var securityToken = handler.CreateToken(new SecurityTokenDescriptor()
                {
                    Issuer = _tokenOptions.Issuer,
                    Audience = _tokenOptions.Audience,
                    SigningCredentials = _tokenOptions.SigningCredentials,
                    Subject = identity,
                    Expires = request.Expires
                });

                await Task.Delay(1);

                var token = handler.WriteToken(securityToken);

                var generatedToken = new GeneratedTokenModel
                {
                    Token = token,
                    Valid = true,
                    Uid = uid,
                    Event = request.EventName,
                    Email = request.UserEmail,
                    Created = DateTime.Today
                };

               var savedToken =  await _repository.Add(generatedToken);

                return new TokenResponse
                {
                    Token = token,
                    ErrorMessage = string.Empty,
                    IsError = false,
                    TicketType = request.TicketType,
                    TicketPrice = request.TicketPrice,
                    GeneratedToken = savedToken
                };
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to generate token", exception.ToString());
                return new TokenResponse
                {
                    Token = null,
                    ErrorMessage = "Error while trying to generate token",
                    IsError = true,
                    TicketType = request.TicketType,
                    TicketPrice = request.TicketPrice
                };
            }
        }

        public async Task<EmailTokenResponseModel> GenerateEmailTokenAsync(EmailTokenRequestModel request)
        {
            try
            {
                var handler = new JwtSecurityTokenHandler();

                // Here, you should create or look up an identity for the user which is being authenticated.
                // For now, just creating a simple generic identity.
                var identity = new ClaimsIdentity(new GenericIdentity(request.UserEmail, "EventNameToken"));

                var securityToken = handler.CreateToken(new SecurityTokenDescriptor()
                {
                    Issuer = _tokenOptions.Issuer,
                    Audience = _tokenOptions.Audience,
                    SigningCredentials = _tokenOptions.SigningCredentials,
                    Subject = identity,
                    Expires = request.Expires
                });

                await Task.Delay(1);
                return new EmailTokenResponseModel
                {
                    Token = handler.WriteToken(securityToken),
                    ErrorMessage = string.Empty,
                    IsError = false
                };
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to generate token", exception.ToString());
                return new EmailTokenResponseModel
                {
                    Token = null,
                    ErrorMessage = "Error while trying to generate token",
                    IsError = true
                };
            }
        }

        public async Task<TokensResponse> GenerateTokens(TokensRequest requests)
        {
            var response = new TokensResponse();

            try
            {
                var handler = new JwtSecurityTokenHandler();

                foreach (var user in requests.Request)
                {
                    // Here, you should create or look up an identity for the user which is being authenticated.
                    // For now, just creating a simple generic identity.
                    var identity = new ClaimsIdentity(new GenericIdentity(user.UserEmail, "EventNameToken"));

                    var securityToken = handler.CreateToken(new SecurityTokenDescriptor()
                    {
                        Issuer = _tokenOptions.Issuer,
                        Audience = _tokenOptions.Audience,
                        SigningCredentials = _tokenOptions.SigningCredentials,
                        Subject = identity,
                        Expires = user.Expires
                    });

                    var res = new TokenResponse
                    {
                        ErrorMessage = string.Empty,
                        IsError = false,
                        Token = handler.WriteToken(securityToken)
                    };
                    
                    response.Response.Add(res);

                }
                await Task.Delay(1);
                return response;

            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to generate tokens", exception.ToString());
               var res = new TokenResponse
                {
                    Token = null,
                    ErrorMessage = "Error while trying to generate token",
                    IsError = true
                };

                 response.Response.Add(res);

                return response;

            }
        }

        public async Task<VerifyTokenResponse> VerifyTokenAsync(VerifyTokenRequest request)
        {
            try
            {
                var tokenParts = request.TokenPlainText.Split('.');
                var clientId = Configuration["azureKeyVault:clientId"];
                var url = Configuration["azureKeyVault:vault"];
                url = $"https://{url}.vault.azure.net/";
                var secret = Configuration["azureKeyVault:clientSecret"];
                var secretIdentifier = "https://keyvault-tokendispenser.vault.azure.net/secrets/kvcert/f0d6c87823d74d97821f4abecfc00ca1";

                //checking to see if token has expired or not
                var handler = new JwtSecurityTokenHandler();
                var ts = handler.ReadJwtToken(request.TokenPlainText);
             
                var cert2 = GetCertificateKeyVault(secretIdentifier, url, clientId, secret).Result;

                //check valid date of token to make sure ticket cannot be used past event date
                if (DateTime.Now > ts.ValidTo)
                {
                    return new VerifyTokenResponse
                    {
                        IsVerified = false,
                        ErrorMessage = "Error, token has expired.",
                        IsError = true
                    };
                }

                //chek valid issuer name
                if (!ts.Issuer.Equals(cert2.Issuer))
                {
                    return new VerifyTokenResponse
                    {
                        IsVerified = false,
                        ErrorMessage = "Error, Issuer name is invalid.",
                        IsError = true
                    };
                }

                //retrieve uid and find row in database
                var token = await _repository.FindByHash(ts.Claims.FirstOrDefault(v => v.Type.Equals("Uid"))?.Value);


                //first check to see if it exists
                if (token == null)
                {
                    return new VerifyTokenResponse
                    {
                        IsVerified = false,
                        ErrorMessage = "token does not exist",
                        IsError = true
                    };
                }

                //check to see if it has been used or not
                if (!token.Valid)
                {
                    return new VerifyTokenResponse
                    {
                        IsVerified = false,
                        ErrorMessage = "token has been used",
                        IsError = true
                    };
                }

                var rsa = (RSACryptoServiceProvider)cert2.PublicKey.Key;
                var sha256 = SHA256.Create();
                var hash = sha256.ComputeHash(Encoding.UTF8.GetBytes(tokenParts[0] + '.' + tokenParts[1]));
                var rsaDeformatter = new RSAPKCS1SignatureDeformatter(rsa);


                rsaDeformatter.SetHashAlgorithm("SHA256");
                if (rsaDeformatter.VerifySignature(hash, FromBase64Url(tokenParts[2])))
                {
                    await Task.Delay(1);
                }


                token.Valid = false;
                token.VerifiedAt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                await _repository.Update(token);

                return new VerifyTokenResponse { IsVerified = true, IsError = false, ErrorMessage = string.Empty };
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to verify token", exception.ToString());
                return new VerifyTokenResponse
                {
                    IsVerified = false,
                    ErrorMessage = "Contact Support",
                    IsError = true
                };
            }
        }

        public async Task<List<GeneratedTokenModel>> RetrieveTokenAsync(string code)
        {
            try
            {
                var result = new List<GeneratedTokenModel>();

                var query = await _repository.FindByCode(code);

                foreach (var item in query)
                {
                    result.Add(item);
                }

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError("Error while trying to verify token", e.ToString());
                return null;
            }
        }

        public async Task<VerifyTokenResponse> VerifyCodeAsync(string code)
        {
            try
            {
                var token = await _repository.FindByHash(code);

                //first check to see if it exists
                if (token == null)
                {
                    return new VerifyTokenResponse
                    {
                        IsVerified = false,
                        ErrorMessage = "token does not exist",
                        IsError = true
                    };
                }

                //check to see if it has been used or not
                if (!token.Valid)
                {
                    return new VerifyTokenResponse
                    {
                        IsVerified = false,
                        ErrorMessage = "token has been used",
                        IsError = true
                    };
                }

                token.Valid = false;
                token.VerifiedAt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                await _repository.Update(token);

                return new VerifyTokenResponse { IsVerified = true, IsError = false, ErrorMessage = string.Empty };
            }
            catch (Exception e)
            {
                _logger.LogError("Error while trying to verify code", e.ToString());
                return new VerifyTokenResponse
                {
                    IsVerified = false,
                    ErrorMessage = "Contact Support",
                    IsError = true
                };
            }
        }

        public async Task<VerifyTokenResponse> VerifyEmailTokenAsync(VerifyTokenRequest request)
        {
            try
            {
                var tokenParts = request.TokenPlainText.Split('.');
                var clientId = Configuration["azureKeyVault:clientId"];
                var url = Configuration["azureKeyVault:vault"];
                url = $"https://{url}.vault.azure.net/";
                var secret = Configuration["azureKeyVault:clientSecret"];
                var secretIdentifier = "https://keyvault-tokendispenser.vault.azure.net/secrets/kvcert/f0d6c87823d74d97821f4abecfc00ca1";

                //checking to see if token has expired or not
                var handler = new JwtSecurityTokenHandler();
                var ts = handler.ReadJwtToken(request.TokenPlainText);

                var cert2 = GetCertificateKeyVault(secretIdentifier, url, clientId, secret).Result;


                var rsa = (RSACryptoServiceProvider)cert2.PublicKey.Key;
                var sha256 = SHA256.Create();
                var hash = sha256.ComputeHash(Encoding.UTF8.GetBytes(tokenParts[0] + '.' + tokenParts[1]));
                var rsaDeformatter = new RSAPKCS1SignatureDeformatter(rsa);


                rsaDeformatter.SetHashAlgorithm("SHA256");
                if (rsaDeformatter.VerifySignature(hash, FromBase64Url(tokenParts[2])))
                {
                    await Task.Delay(1);
                }

                return new VerifyTokenResponse { IsVerified = true, IsError = false, ErrorMessage = string.Empty };
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to verify token", exception.ToString());
                return new VerifyTokenResponse
                {
                    IsVerified = false,
                    ErrorMessage = "Contact Support",
                    IsError = true
                };
            }
        }

        private static byte[] FromBase64Url(string base64Url)
        {
            var padded = base64Url.Length % 4 == 0
                ? base64Url : base64Url + "====".Substring(base64Url.Length % 4);
            var base64 = padded.Replace("_", "/")
                                  .Replace("-", "+");
            return Convert.FromBase64String(base64);
        }

        private static async Task<X509Certificate2> GetCertificateKeyVault(string secretIdentifier, string url, string clientId, string secret)
        {

            var util = new KeyVaultUtil(url, clientId, secret);
            var client = new KeyVaultClient(util.GetAccessToken);
            var vaultSecret = await client.GetSecretAsync(secretIdentifier);

            var certSecret = new X509Certificate2(
                Convert.FromBase64String(vaultSecret.Value),
                string.Empty,
                X509KeyStorageFlags.MachineKeySet);

            return certSecret;
        }
    }
}
