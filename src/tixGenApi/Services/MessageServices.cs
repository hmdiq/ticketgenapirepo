﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SendGrid;
using SendGrid.Helpers.Mail;
using tixGenApi.Interfaces;

namespace tixGenApi.Services
{
    // This class is used by the application to send Email and SMS
    // when you turn on two-factor authentication in ASP.NET Identity.
    // For more details see this link http://go.microsoft.com/fwlink/?LinkID=532713
    public class AuthMessageSender : IEmailSender
    {
        private readonly ILogger<AuthMessageSender> _logger;
        private string apikey = "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";

        public AuthMessageSender(ILogger<AuthMessageSender> logger)
        {
            _logger = logger;
        }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            // Plug in your email service here to send an email.
            try
            {
                var client = new SendGridClient(apikey);

                var msg = new SendGridMessage();

                msg.SetFrom(new EmailAddress("ticketfort@heuristixs.com", "Ticket Fort"));

                var recipients = new List<EmailAddress>
                {
                    new EmailAddress(email)
                };

                msg.AddTos(recipients);

                msg.SetSubject(subject);
                msg.AddContent(MimeType.Html, message);

                await client.SendEmailAsync(msg);

            }
            catch (Exception ex)
            {
                _logger.LogError("Error sending mail", ex.ToString());

            }
            await Task.FromResult(0);
        }

        public async Task SendEmailWithAttachementAsync(string email, string subject, string message, List<string> files)
        {
            // Plug in your email service here to send an email.
            try
            {
                var client = new SendGridClient(apikey);

                var msg = new SendGridMessage();

                msg.SetFrom(new EmailAddress("ticketfort@heuristixs.com", "Ticket Fort"));

                var recipients = new List<EmailAddress>
                {
                    new EmailAddress(email)
                };

                msg.AddTos(recipients);

                msg.SetSubject(subject);
                msg.AddContent(MimeType.Html, message);

                var counter = 1;
                foreach (var file in files)
                {
                    var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
                    var pdfBytes1 = htmlToPdf.GeneratePdf(file);

                    msg.AddAttachment("Ticket"+counter + ".pdf", Convert.ToBase64String(pdfBytes1), "application/pdf");
                    counter = counter + 1;
                }
  

                var res = await client.SendEmailAsync(msg);

                if (res.StatusCode != HttpStatusCode.Accepted)
                {
                    _logger.LogError("Error sending mail");
                }

            }
            catch (Exception ex)
            {
                _logger.LogError("Error sending mail", ex.ToString());

            }
            await Task.FromResult(0);
        }

        public async Task SendEmailSupportAsync(string email, string subject, string message)
        {
            try
            {
                var client = new SendGridClient(apikey);


                var msg = new SendGridMessage();

                msg.SetFrom(new EmailAddress("ticketfort@heuristixs.com", "Ticket Fort"));

                var recipients = new List<EmailAddress>
                {
                    new EmailAddress("support@heuristixs.com")
                };

                msg.AddTos(recipients);

                msg.SetSubject(subject);
                msg.AddContent(MimeType.Html, message);

                await client.SendEmailAsync(msg);

            }
            catch (Exception ex)
            {
                _logger.LogError("Error sending mail", ex.ToString());

            }
            await Task.FromResult(0);
        }

        public async Task SendEmailConfirmationAsync(string email, string subject, string message)
        {
            try
            {
                var client = new SendGridClient(apikey);


                var msg = new SendGridMessage();

                msg.SetFrom(new EmailAddress("ticketfort@heuristixs.com", "Ticket Fort"));

                var recipients = new List<EmailAddress>
                {
                    new EmailAddress(email)
                };

                msg.AddTos(recipients);

                msg.SetSubject(subject);
                msg.AddContent(MimeType.Html, message);

                await client.SendEmailAsync(msg);

            }
            catch (Exception ex)
            {
                _logger.LogError("Error sending mail", ex.ToString());

            }
            await Task.FromResult(0);
        }

        public Task SendSmsAsync(string number, string message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }

        public FileContentResult ByteToFile(byte[] data)
        {
            
            return new FileContentResult(data, "application/pdf");
        }

   
    }
}
