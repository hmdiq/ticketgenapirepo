﻿using System;

namespace tixGenApi.Models
{
    public class EmailTokenRequestModel
    {
        public string UserEmail { get; set; }
        public DateTime? Expires { get; set; }
    }
}
