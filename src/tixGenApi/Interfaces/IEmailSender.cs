﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace tixGenApi.Interfaces
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);

        Task SendEmailWithAttachementAsync(string email, string subject, string message, List<string> files);

        Task SendEmailSupportAsync(string email, string subject, string message);

        Task SendEmailConfirmationAsync(string email, string subject, string message);
    }
}
