﻿namespace tixGenApi.Config
{
    public class CertificateConfiguration
    {
        public SigningCertificate SigningCertificate { get; set; }
    }
}
