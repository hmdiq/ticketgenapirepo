﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using tixGenApi.Interfaces;
using tixGenApi.Models;
using ZXing;

namespace tixGenApi.Services
{
    public class QrCodeService : IQrCode
    {
        private readonly ILogger<QrCodeService> _logger;

        public QrCodeService(ILogger<QrCodeService> logger)
        {
            _logger = logger;
        }

        public async Task<WriteQrCodeResponse> WriteQrCode(WriteQrCodeRequest request)
        {
            try
            {
                // encode the string as PDF417
                var barcodeWriter = new BarcodeWriter
                {
                    Format = BarcodeFormat.QR_CODE,
                    Options = new ZXing.Common.EncodingOptions
                    {
                        Height = 360,
                        Width = 360,
                        Margin = 1
                        
                    },
                };

                var bitmap = barcodeWriter.Write(barcodeWriter.Encode(request.TokenPlainText));
                byte[] bitmaptopng;
                using (var stream = new MemoryStream())
                {
                    bitmap.Save(stream, ImageFormat.Png);
                    bitmaptopng = stream.ToArray();
                }
                var base64QrData = Convert.ToBase64String(bitmaptopng);
                var imageDataUrl = $"data:image/png;base64,{base64QrData}";
                await Task.Delay(1);
                return new WriteQrCodeResponse
                {
                    ErrorMessage = string.Empty,
                    IsError = false,
                    ImageBase64QrData = imageDataUrl
                };

            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to generate qrcode", exception.ToString());
                return new WriteQrCodeResponse
                {
                    ErrorMessage = "Error while trying to generate qrcode",
                    IsError = true,
                    ImageBase64QrData = null
                };
            }
        }

        public async Task<WriteQrCodesResponse> WriteQrCodes(WriteQrCodesRequest request)
        {
            Bitmap bitmaps = null;
            try
            {
                // encode the string as PDF417
                var barcodeWriter = new BarcodeWriter
                {
                    Format = BarcodeFormat.QR_CODE,
                    Options = new ZXing.Common.EncodingOptions
                    {
                        Height = 210,
                        Width = 862,
                        Margin = 5
                    },
                };

                foreach(var token in request.Tokens)
                {
                    bitmaps = barcodeWriter.Write(barcodeWriter.Encode(token));
                }
               
                await Task.Delay(1);
                return new WriteQrCodesResponse
                {
                    ErrorMessage = string.Empty,
                    IsError = false,
                    QrCodeBitmap = bitmaps
                };
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to generate qrcodes", exception.ToString());
                return new WriteQrCodesResponse
                {
                    ErrorMessage = "Error while trying to generate qrcodes",
                    IsError = true,
                    QrCodeBitmap = null
                };
            }
        }
    }
}
