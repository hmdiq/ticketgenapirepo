﻿using System.Collections.Generic;
using System.Threading.Tasks;
using tixGenApi.Models;

namespace tixGenApi.Interfaces
{
    public interface ITokens
    {
        Task<TokenResponse> GenerateTokenAsync(TokenRequest request);
        Task<EmailTokenResponseModel> GenerateEmailTokenAsync(EmailTokenRequestModel request);
        Task<VerifyTokenResponse> VerifyTokenAsync(VerifyTokenRequest request);
        Task<List<GeneratedTokenModel>> RetrieveTokenAsync(string code);

        Task<VerifyTokenResponse> VerifyCodeAsync(string code);

        Task<VerifyTokenResponse> VerifyEmailTokenAsync(VerifyTokenRequest request);
        Task<TokensResponse> GenerateTokens(TokensRequest requests);
    }
}
