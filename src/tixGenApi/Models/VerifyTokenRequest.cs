﻿namespace tixGenApi.Models
{
    public class VerifyTokenRequest
    {
        public string TokenPlainText { get; set; }
    }
}
