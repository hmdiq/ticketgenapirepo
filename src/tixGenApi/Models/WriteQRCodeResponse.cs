﻿namespace tixGenApi.Models
{
    public class WriteQrCodeResponse
    {
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
        public string ImageBase64QrData { get; set; }
    }
}
