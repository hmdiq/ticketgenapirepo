﻿namespace tixGenApi.Models
{
    public class SignTokenRequest
    {
        public string Token { get; set; }
    }
}
