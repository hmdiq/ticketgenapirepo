﻿using System.Collections.Generic;

namespace tixGenApi.Models
{
    public class GenerateTicketsResponse
    {
        public GenerateTicketsResponse()
        {
            Responses = new List<GenerateTicketResponse>();
        }
        public List<GenerateTicketResponse> Responses { get; set; }
    }
}
