﻿using System;

namespace tixGenApi.Models
{
    public class GeneratedTokenModel : BaseEntity
    {
        public string Token { get; set; }

        public string Uid { get; set; }

        public bool Valid { get; set; }

        public string Email { get; set; }

        public string Event { get; set; }

        public string VerifiedAt { get; set; } = string.Empty;
    }
}
