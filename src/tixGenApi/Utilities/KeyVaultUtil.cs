﻿using System;
using System.Threading.Tasks;
using Microsoft.Azure.KeyVault;
using Microsoft.IdentityModel.Clients.ActiveDirectory;

namespace tixGenApi.Utilities
{
    public class KeyVaultUtil
    {

        private string _vaultAddress;
        private readonly string _clientId;
        private readonly string _clientSecret;

        public KeyVaultUtil(string vaultAddress, string clientId, string clientSecret)
        {
            _vaultAddress = vaultAddress;
            _clientId = clientId;
            _clientSecret = clientSecret;
        }

        public string GetKeyVaultSecret(string secretNode, string secretUri)
        {

            var keyVaultClient = new KeyVaultClient(GetAccessToken);
            return keyVaultClient.GetSecretAsync(secretUri).Result.Value;
        }

        public async Task<string> GetAccessToken(string authority, string resource, string scope)
        {
            var authContext = new AuthenticationContext(authority);
            var clientCred = new ClientCredential(_clientId, _clientSecret);
            var result = await authContext.AcquireTokenAsync(resource, clientCred);

            if (result == null)
                throw new InvalidOperationException("Failed to obtain the JWT token");

            return result.AccessToken;
        }
    }
}
