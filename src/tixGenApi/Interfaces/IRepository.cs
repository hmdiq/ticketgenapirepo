﻿using System.Linq;
using System.Threading.Tasks;

namespace tixGenApi.Interfaces
{
    public interface IRepository<T> where T : class
    {

        Task<IQueryable<T>> GetAll();
        Task<T> Add(T entity);
        Task Delete(T entity);
        Task<T> FindByHash(string hash);

        Task<IQueryable<T>> FindByCode(string hash);

        Task Update(T entity);

    }
}
