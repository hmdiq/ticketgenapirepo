﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using tixGenApi.Config;
using tixGenApi.Interfaces;
using tixGenApi.Models;
using Microsoft.AspNetCore.Authorization;

namespace tixGenApi.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    public class TokenController : Controller
    {

        private readonly ILogger<TokenController> _logger;
        private readonly ITokens _token;
        private readonly IQrCode _qrcode;
        private readonly ICryptoSymmetric _symmetric;
        private readonly ITickets _ticket;

        public TokenController(ILogger<TokenController> logger,
            IHostingEnvironment environment,
            ITokens token,
            IQrCode qrcode,
            ICryptoSymmetric symmetric,
            ITickets ticket,
            IOptionsSnapshot<AppConfiguration> appConfiguration,
            IOptionsSnapshot<CertificateConfiguration> certificateConfig,
            IConfiguration configuration)
        {
            _logger = logger;
            _token = token;
            _qrcode = qrcode;
            _symmetric = symmetric;
            _ticket = ticket;
        }

        [HttpPost]
        [Route("createtoken")]
        public async Task<IActionResult> GetToken([FromBody]TokenRequest model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var token = await _token.GenerateTokenAsync(model);
                    return Ok(token);
                }
                else
                {
                    var response = new TokenResponse
                    {
                        ErrorMessage = "Invalid request",
                        IsError = true,
                        Token = null
                    };
                    return NotFound(response);
                }
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to GetToken", exception.ToString());
                return NotFound();
            }
        }

        [HttpPost]
        [Route("createtokens")]
        public async Task<IActionResult> GetTokens([FromBody]TokensRequest model)
        {
            try
            {
                if (!ModelState.IsValid) return NotFound(null);
                var token = await _token.GenerateTokens(model);
                return Ok(token);
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to GetToken", exception.ToString());
                return NotFound();
            }
        }

        [HttpPost]
        [Route("createtokenemail")]
        public async Task<IActionResult> GetEmailToken([FromBody]EmailTokenRequestModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var token = await _token.GenerateEmailTokenAsync(model);
                    return Ok(token);
                }
                else
                {
                    var response = new EmailTokenResponseModel
                    {
                        ErrorMessage = "Invalid request",
                        IsError = true,
                        Token = null
                    };
                    return NotFound(response);
                }
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to GetToken", exception.ToString());
                return NotFound();
            }
        }

        [HttpPost]
        [Route("verifytoken")]
        public async Task<IActionResult> VerifyToken([FromBody]VerifyTokenRequest model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var response = await _token.VerifyTokenAsync(model);
                    return Ok(response);
                }
                else
                {
                    var response = new VerifyTokenResponse
                    {
                        ErrorMessage = "Invalid request",
                        IsError = true,
                        IsVerified = false
                    };
                    return NotFound(response);
                }
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to verify token", exception.ToString());
                return NotFound();
            }
        }

        [HttpPost]
        [Route("verifyemailtoken")]
        public async Task<IActionResult> VerifyEmailToken([FromBody]VerifyTokenRequest model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var response = await _token.VerifyEmailTokenAsync(model);
                    return Ok(response);
                }
                else
                {
                    var response = new VerifyTokenResponse
                    {
                        ErrorMessage = "Invalid request",
                        IsError = true,
                        IsVerified = false
                    };
                    return NotFound(response);
                }
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to verify token", exception.ToString());
                return NotFound();
            }
        }

        [HttpPost]
        [Route("retrievetoken")]
        public async Task<IActionResult> RetrieveToken(string code)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var response = await _token.RetrieveTokenAsync(code);
                    return Ok(response);
                }
                else
                {
                    var response = new VerifyTokenResponse
                    {
                        ErrorMessage = "Invalid request",
                        IsError = true,
                        IsVerified = false
                    };
                    return NotFound(response);
                }
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to verify token", exception.ToString());
                return NotFound();
            }
        }

        [HttpPost]
        [Route("verifycode")]
        public async Task<IActionResult> VerifyCode(string code)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var response = await _token.VerifyCodeAsync(code);
                    return Ok(response);
                }
                else
                {
                    var response = new VerifyTokenResponse
                    {
                        ErrorMessage = "Invalid request",
                        IsError = true,
                        IsVerified = false
                    };
                    return NotFound(response);
                }
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to verify token", exception.ToString());
                return NotFound();
            }
        }

        //POST api/values
        [HttpPost]
        [Route("createqrcode")]
        public async Task<IActionResult> CreateQrCode([FromBody]WriteQrCodeRequest model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var qrcode = await _qrcode.WriteQrCode(model);
                    return Ok(qrcode);
                }
                var response = new WriteQrCodeResponse
                {
                    ErrorMessage = "Invalid request",
                    IsError = true,
                    ImageBase64QrData = string.Empty
                };
                return NotFound(response);
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while create qrcode", exception.ToString());
                return NotFound();
            }
        }

        //POST api/values
        [HttpPost("encrypttoken")]
        public async Task<IActionResult> EncryptToken([FromBody]EncryptionRequest model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var cipher = await _symmetric.DataProtect(model);
                    return Ok(cipher);
                }
                else
                {
                    var cipher = new EncryptionResponse
                    {
                        IsError = true,
                        ErrorMessage = "Invalid request",
                        CypherText = null
                    };
                    return NotFound(cipher);
                }
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while encrypting token", exception.ToString());
                return NotFound();
            }
        }


        //POST api/values
        [HttpPost("decrypttoken")]
        public async Task<IActionResult> DecryptToken([FromBody]DecryptionRequest model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var plain = await _symmetric.DataUnProtect(model);
                    return Ok(plain);
                }
                else
                {
                    var plain = new DecryptionResponse
                    {
                        IsError = false,
                        ErrorMessage = "Invalid request"
                    };
                    return NotFound(plain);
                }
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while decrypting token", exception.ToString());
                return NotFound();
            }
        }


        [HttpPost("generateticket")]
        public async Task<IActionResult> GenerateTicket([FromBody]GenerateTicketRequest model)
        {      
            try
            {
                if (ModelState.IsValid)
                {
                    var ticket = await _ticket.GenerateTicketAsync(model);
                    return Ok(ticket);
                }
                var response = new GenerateTicketResponse
                {
                    ErrorMessage = "Invalid request",
                    IsError = true
                };
                return NotFound(response);
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to generate ticket", exception.ToString());
                return NotFound();
            }
        }

        [HttpPost("generatetickets")]
        public async Task<IActionResult> GenerateTickets([FromBody]GenerateTicketsRequest model)
        {
            try
            {
                if (!ModelState.IsValid) return NotFound(null);
                var ticket = await _ticket.GenerateTicketsAsync(model);
                return Ok(ticket);
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to generate tickets", exception.ToString());
                return NotFound();
            }
        }
    }
}
