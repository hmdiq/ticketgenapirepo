﻿namespace tixGenApi.Models
{
    public class EncryptionResponse
    {
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
        public string CypherText { get; set; }
    }
}
