﻿using System.Collections.Generic;

namespace tixGenApi.Models
{
    public class TokensResponse
    {
        public TokensResponse()
        {
            Response = new List<TokenResponse>();
        }
        public List<TokenResponse> Response { get; set; }

    }
}
