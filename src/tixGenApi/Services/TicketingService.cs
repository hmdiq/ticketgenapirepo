﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using tixGenApi.Interfaces;
using tixGenApi.Models;

namespace tixGenApi.Services
{
    public class TicketingService : ITickets
    {
        private readonly ILogger<TicketingService> _logger;
        private readonly IQrCode _qrservice;
        private readonly ICryptoSymmetric _cryptoservice;
        private readonly ITokens _tokenservice;
        private readonly IRepository<GeneratedTokenModel> _repository;
        private readonly IEmailSender _msgService;

        public TicketingService(ILogger<TicketingService> logger,
                                IQrCode qrservice,
                                ICryptoSymmetric cryptoservice,
                                IRepository<GeneratedTokenModel> repository,
                                ITokens tokenservice,
            IEmailSender msgService)
        {
            _logger = logger;
            _qrservice = qrservice;
            _cryptoservice = cryptoservice;
            _tokenservice = tokenservice;
            _repository = repository;
            _msgService = msgService;
        }

        public async Task<GenerateTicketResponse> GenerateTicketAsync(GenerateTicketRequest request)
        {
            try
            {
                var token = await _tokenservice.GenerateTokenAsync(request.Request);
                await Task.Delay(1);
                if (!token.IsError)
                {
                    var encryptRequest = new EncryptionRequest
                    {
                        TokenPlainText = token.Token
                    };
                    var cipher = await _cryptoservice.DataProtect(encryptRequest);
                    if(!cipher.IsError)
                    {
                        var qrRequest = new WriteQrCodeRequest
                        {
                            TokenPlainText = cipher.CypherText
                        };
                        var qrcode = await _qrservice.WriteQrCode(qrRequest);
                        if(!qrcode.IsError)
                        {
                            return new GenerateTicketResponse
                            {
                                QrCode = qrcode.ImageBase64QrData,
                                IsError = false,
                                ErrorMessage = string.Empty
                            };
                        }
                        else
                        {
                            return new GenerateTicketResponse
                            {
                                QrCode = null,
                                IsError = true,
                                ErrorMessage = "Unable to generate code"
                            };
                        }
                    }
                    else
                    {
                        return new GenerateTicketResponse
                        {
                            QrCode = null,
                            IsError = true,
                            ErrorMessage = "Data encryption failed"
                        };
                    }
                }
                else
                {
                    return new GenerateTicketResponse
                    {
                        QrCode = null,
                        IsError = true,
                        ErrorMessage = "Token generation failed"
                    };
                }
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to generate ticket", exception.ToString());
                return new GenerateTicketResponse
                {
                    ErrorMessage = "Error while trying to generate ticket",
                    IsError = true
                };
            }
        }

        public async Task<GenerateTicketsResponse> GenerateTicketsAsync(GenerateTicketsRequest request)
        {
            var response = new GenerateTicketsResponse();
            try
            {
                if (request != null)
                {
                    foreach (var req in request.Requests)
                    {
                        var token = await _tokenservice.GenerateTokenAsync(req);
                        await Task.Delay(1);
                        if (!token.IsError)
                        {                       
                            var qrRequest = new WriteQrCodeRequest
                                {
                                    TokenPlainText = token.Token
                                };
                                var qrcode = await _qrservice.WriteQrCode(qrRequest);

                                if (!qrcode.IsError)
                                {
                                   var res = new GenerateTicketResponse
                                    {
                                        QrCode = qrcode.ImageBase64QrData,
                                        IsError = false,
                                        ErrorMessage = string.Empty,
                                        TicketPrice = token.TicketPrice,
                                        TicketType = token.TicketType,
                                        GeneratedToken = token.GeneratedToken
                                    };

                                    response.Responses.Add(res);                           

                            }
                                else
                                {
                                    var res = new GenerateTicketResponse
                                    {
                                        QrCode = null,
                                        IsError = true,
                                        ErrorMessage = "Unable to generate code",
                                        TicketPrice = token.TicketPrice,
                                        TicketType = token.TicketType
                                    };
                                    response.Responses.Add(res);
                                }
                        }
                    }

                    return response;
                }
                return response;
            }
            catch (Exception e)
            {
               _logger.LogError("Error Making Multiple Tickets"+e);
                await _msgService.SendEmailSupportAsync("", "Error From Ticket Fort", "Error in tickets generation:" + e.Message);
                return response;
            }
        }

        public async Task<GenerateTicketResponse> GenerateTicketLowLatencyAsync(GenerateTicketRequest request)
        {
            try
            {
                var token = await _tokenservice.GenerateTokenAsync(request.Request);
                await Task.Delay(1);
                if (!token.IsError)
                {
                    var encryptRequest = new EncryptionRequest
                    {
                        TokenPlainText = token.Token
                    };
                    var cipher = await _cryptoservice.DataProtect(encryptRequest);
                    if (!cipher.IsError)
                    {
                        var qrRequest = new WriteQrCodeRequest
                        {
                            TokenPlainText = cipher.CypherText
                        };
                        var qrcode = await _qrservice.WriteQrCode(qrRequest);
                        if (!qrcode.IsError)
                        {
                            return new GenerateTicketResponse
                            {
                                QrCode = qrcode.ImageBase64QrData,
                                IsError = false,
                                ErrorMessage = string.Empty
                            };
                        }
                        else
                        {
                            return new GenerateTicketResponse
                            {
                                QrCode = null,
                                IsError = true,
                                ErrorMessage = "Unable to generate code"
                            };
                        }
                    }
                    else
                    {
                        return new GenerateTicketResponse
                        {
                            QrCode = null,
                            IsError = true,
                            ErrorMessage = "Data encryption failed"
                        };
                    }
                }
                else
                {
                    return new GenerateTicketResponse
                    {
                        QrCode = null,
                        IsError = true,
                        ErrorMessage = "Token generation failed"
                    };
                }
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to generate ticket", exception.ToString());
                return new GenerateTicketResponse
                {
                    ErrorMessage = "Error while trying to generate ticket",
                    IsError = true
                };
            }
        }
    }
}
