﻿using System.Threading.Tasks;
using tixGenApi.Models;

namespace tixGenApi.Interfaces
{
    public interface ITickets
    {
        Task<GenerateTicketResponse> GenerateTicketAsync(GenerateTicketRequest request);
        Task<GenerateTicketResponse> GenerateTicketLowLatencyAsync(GenerateTicketRequest request);
        Task<GenerateTicketsResponse> GenerateTicketsAsync(GenerateTicketsRequest requests);
    }
}
