﻿namespace tixGenApi.Config
{
    public class AzureKeyVault
    {
        public string Vault { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}
