﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using tixGenApi.Data;
using tixGenApi.Interfaces;
using tixGenApi.Models;

namespace tixGenApi.Services
{
    public class Repository :IRepository<GeneratedTokenModel>
    {
        private readonly ILogger<GeneratedTokenModel> _logger;
        private readonly ApiContext _entities;
        public Repository(ApiContext entities, ILogger<GeneratedTokenModel> logger)
        {
            _entities = entities;
            _logger = logger;
        }

        public async Task<IQueryable<GeneratedTokenModel>> GetAll()
        {
            IQueryable<GeneratedTokenModel> query = _entities.Set<GeneratedTokenModel>();
            await Task.FromResult(TimeSpan.FromMilliseconds(1));
            return query;
        }

        public async Task<GeneratedTokenModel> Add(GeneratedTokenModel entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            _entities.Set<GeneratedTokenModel>().Add(entity);
            await _entities.SaveChangesAsync();

            return entity;
        }

        public Task Delete(GeneratedTokenModel entity)
        {
            throw new NotImplementedException();
        }

        public async Task<GeneratedTokenModel> FindByHash(string uid)
        {
            try
            {
                var result = await _entities.Tokens.FirstOrDefaultAsync(x => x.Uid.Equals(uid));
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError("exception" + ex);
                return null;
            }
        }

        public async Task<IQueryable<GeneratedTokenModel>> FindByCode(string code)
        {
            try
            {
                var query = _entities.Set<GeneratedTokenModel>().Where(t => t.Uid.EndsWith(code));
                await Task.FromResult(TimeSpan.FromMilliseconds(1));
                return query;
            }
            catch (Exception ex)
            {
                _logger.LogError("exception" + ex);
                return null;
            }
        }

        public async Task Update(GeneratedTokenModel entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            _entities.Entry(entity).State = EntityState.Modified;
            await _entities.SaveChangesAsync();

            await Task.FromResult(TimeSpan.FromMilliseconds(1));
        }
    }
}
